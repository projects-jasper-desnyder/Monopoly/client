"use strict";

const _config = {
    prefix: 'monopoly',
    pawns: ["blaze", "cow", "creeper", "enderman", "guardian", "pig", "sheep", "skeleton", "slime", "spider", "steve", "villager", "wither", "zombie"],
    playersAndPawns: [],
    errorHandlerSelector: '.errormessages p',
    getAPIUrl: function () { return `localhost/api`; }
};

